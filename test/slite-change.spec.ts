import { SliteChange } from '../src/slite-change';

const assert = require('assert');
let fs = require('fs');
let sqlite = require('sqlite3').verbose();

let DbFile = 'test_database.db';

function ChangeSync(DbFile: string, DbChanges: string) {
	return new Promise((resolve, reject) => {
		// if (fs.existsSync(DbFile)) {
		// 	fs.unlinkSync(DbFile, (err) => {});
		// }
		setTimeout( () => {
			return
		},	45);
		let Sql = new sqlite.Database(DbFile, (err: any) => {
			if (err) {
				console.error('Could not connect to database', err);
				resolve('Could not connect to database');
			} else {
				console.info('Connected to database');
				new SliteChange(Sql, DbChanges, (ReturnMsg: string) => {
					if(ReturnMsg.length != 0) {
						console.error('ReturnMsg: '+ReturnMsg);
					}
					resolve(ReturnMsg);
				});
			}
		});
	});
}

function DeleteTempFiles() {
	if (fs.existsSync('./db-changes/999-db-change.sql')) {
		console.log('Delete tempoary SQL')
		fs.unlinkSync('./db-changes/999-db-change.sql', (err: any) => {});
	}
	if(fs.existsSync(DbFile)) {
		console.log('Delete %s', DbFile);
		fs.unlinkSync(DbFile, (err: any) => {});
	}

}

describe('Testing database change control', () => {
	after( () => {
		setTimeout( () => {
			DeleteTempFiles();
		}, 1000);
	});
	it('Should create a new database from samples', () => {
		return ChangeSync(DbFile, './db-changes/').then(result => {
			console.log('res = "%s"', result);
			assert.equal(result, '', 'Returnded a message thing');
		});
    })//.timeout(5000);
	it('Should return a fail from SQL syntax error.', () => {
		fs.writeFileSync('./db-changes/999-db-change.sql', 'This file is going to fail',  (err: any) => {
            if (err) {
                return console.error(err);
            }
        });
		return ChangeSync(DbFile, './db-changes/').then(result => {
			assert.equal(result, 'Invalid change set', 'Returnded a message thing');
		});
	})//.timeout(5000);
});

// https://dev.to/adamcoster/beware-silently-skipped-tests-in-mocha-449h
// describe('My test', function(){
//     it.only('will run this test', function(){});
//     it('will not run this test', function(){});
//   });