--changeset Royce:3
BEGIN;
CREATE TABLE 'aclgroup' (
	'id' INT NOT NULL,
	'description' CHAR(50),
	PRIMARY KEY ('id'));

INSERT INTO 'aclgroup' ('id') VALUES (0);

ALTER TABLE 'users' ADD COLUMN 'roomNumber' CHAR(20);

CREATE TABLE 'aclassignments' (
	'id' INT NOT NULL,
	'userid' INT NOT NULL,
	'aclgroup' INT NOT NULL,
	PRIMARY KEY ('id'));

INSERT INTO 'aclassignments' ('id', 'userid', 'aclgroup') VALUES (0, 0, 0);
COMMIT;