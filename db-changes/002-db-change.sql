--changeset Royce:2
BEGIN;
CREATE TABLE 'menus' (
	'id' INT NOT NULL,
	'type' CHAR(50),
	'parent' INT DEFAULT 0,
	'text' VARCHAR(100),
	'link' VARCHAR(1000),
	aclgroup INT DEFAULT null,
	PRIMARY KEY ('id'));
	
INSERT INTO 'menus' ('id') VALUES (0);

INSERT INTO menus (id, type, parent, text, link) VALUES ((SELECT MAX(id)+1 FROM menus), 'link', 0, 'Google', 'https://google.com');
INSERT INTO menus (id, type, parent, text, link) VALUES ((SELECT MAX(id)+1 FROM menus), 'link', 0, 'YouTube', 'https://youtube.com');
INSERT INTO menus (id, type, parent, text, link) VALUES ((SELECT MAX(id)+1 FROM menus), 'link', 0, 'GitLab', 'https://gitlab.com');

COMMIT;