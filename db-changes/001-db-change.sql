--changeset Royce:1
BEGIN;
CREATE TABLE 'users' (
  'id' int NOT NULL,
  'firstname' VARCHAR(50),
  'lastname' VARCHAR(50),
  'username' VARCHAR(50),
  'passwd' VARCHAR(50),
  'emailAddress' VARCHAR(50),
  'passChanged' INT,
  'passExpire' INT,
  PRIMARY KEY ('id'));

INSERT INTO 'users' (id, firstname, lastname, username, passwd, emailAddress) VALUES (0, 'admin', 'admin', 'admin', 'password', 'roo@localhost');
UPDATE 'users' SET lastname='admin', username='admin', passChanged=(SELECT strftime('%s', 'now'));


CREATE TABLE 'config' (
  'parameter' VARCHAR(45) NULL,
  'value' VARCHAR(45) NULL,
  PRIMARY KEY ('parameter'));
  
INSERT INTO 'config' ('parameter','value') VALUES ('db_version', '1');
COMMIT;