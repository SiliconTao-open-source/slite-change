"use strict";

let fs = require('fs');
let sqlite = require('sqlite3').verbose();
let date = require('date-and-time');
let md5 = require('md5');
// import { nextTick } from 'process';

type CallStack = {
	id: number;
	previous: number;
	author: string;
	sql: string;
	file: string;
	procedures: string[];
};

type ProcStack = {
	procName: string;
	procFunc: (args: string) => void;
}

/**
 * While creating a modern Angular web service to run on Raspberry Pi, a DB change control was needed.
 * Liquibase is great and it was consided for use but in a small system were Sqlite is used,
 * having a large Java application just to manage simple changes may not be practical.
 * Liquibase is recommended for system that use larger DB engines like MySQL or PostgreSQL.
 */
export class SliteChange {
	public Fs: unknown;
	public Sql: typeof sqlite.Database;
	public ChangeLog: string;
	private ChangeLogDir: string;
	private LastCompletedChangeId: number;
	private ErrorMessage: string;
	private ErrorFile: string;
	private ChangesIssued: number;
	private ChangesCompleted: number;
	private callStack: any;
	private sourceFiles: number;
	private ProcCallStack: ProcStack[];

	constructor(Sql: typeof sqlite.Database, ChangeLog: string, Callback?: any, ProcCallStack?: ProcStack[]) {
		this.ChangeLog = ChangeLog;
		this.ChangeLogDir = ChangeLog;
		this.Sql = Sql;
		this.LastCompletedChangeId = 0;
		this.ErrorMessage = '';
		this.ErrorFile = ''
		this.ChangesIssued = 0;
		this.ChangesCompleted = 0;
		this.callStack = new Array();
		this.sourceFiles = 0;
		this.ProcCallStack = [];
		if(ProcCallStack) {
			this.ProcCallStack = ProcCallStack;
		}

		this.ChangeLogTable((errMsg: string) => {
			if(errMsg != '') {
				Callback(errMsg);
			} else {
				this.ReadChangeFiles((errorMsg: string) => {
					console.log('main err "%s", files %d, Issued %d, Completed %d, callStack length %d',
					 	errorMsg, this.sourceFiles, this.ChangesIssued, this.ChangesCompleted, this.callStack.length);
					if((errorMsg != '') || ( (this.sourceFiles === this.ChangesIssued))) {
						Callback(errorMsg);
					}
				});
			}
		});
	}

	/**
	 * Hold the first failure
	 * @param NewError
	 */
	private SetError(NewError: string, Callback: any) {
		if(this.ErrorMessage == '') {
			// console.log('SetError called with "%s"', NewError);
			if(this.Sql != undefined) {
				this.Sql.close();
				this.Sql = undefined;
			}
			this.ErrorMessage = NewError;
			console.error(NewError);
			Callback(NewError);
		}
	}

	/**
	 * Read the change log files.
	 * Save the change comment and SQL body
	 * If the change comment has a change ID greater then the current change ID, execute SQL and save record to change log.
	 * Continue reading the change log file.
	 */
	private ReadChangeFiles(Callback: any) {
		let PreviousStack = 0;
		// ChangeAuthor, ChangeNumber, Buffer should be a type.
		// And the values should be added to an array that sends off in the end.
		fs.readdir(this.ChangeLogDir, (err: any, files: any ) => {
			if (err) {
				this.SetError('Unable to scan directory: ' + err, Callback);
			}
			this.sourceFiles = files.length;
			files.sort().forEach( (file: string) => {
				fs.readFile(this.ChangeLogDir + file, (err: any, data: any) => {
					if(err) {
						this.SetError('Error in change file "'+file+'": '+err.message, Callback);
					} else {
						let Buffer = '';
						let ChangeAuthor = '';
						let ChangeNumber = '';
						let Procedures: string[] = [];
						let SqlArray = data.toString().split("\n");
						SqlArray.forEach((line: string) => {
							// if not comment or blank line, add to buffer.
							if(line.startsWith('--changeset ')) {
								ChangeAuthor = line.split(/[ :]+/)[1];
								ChangeNumber = line.split(/[ :]+/)[2];
								return // emulates a conginue inside a forEach() loop
							}
							if(line.startsWith('--')) {
								return // comment line
							}
							if(line.startsWith('@Procedure ')) {
								Procedures.push(line.substring('@Procedure '.length));
								return
							}
							Buffer += line;
							Buffer += '\n';
						});
						if((ChangeAuthor == '') || (ChangeNumber == '')) {
							this.SetError('Invalid change set', Callback);
						}

						if(Buffer.length > 0) {
							let Pre = parseInt(ChangeNumber) - 1;
							this.callStack.push({
								id: ChangeNumber,
								previous: Pre,
								author: ChangeAuthor,
								sql: Buffer,
								file: file,
								procedures: Procedures
							});

							if(this.callStack.length === files.length) {
								this.CommitChange(this.callStack.find( (cB: CallStack) => cB.previous == 0), (ErrMsg: string) => {
									Callback(ErrMsg);
								});
							}
						}
					}
				})
			});
		})
	}

	private runProcStack(procStack: string[], CallBack: any) {
		if(procStack.length == 0) {
			CallBack();
		} else {
			procStack.forEach( (pC: string, i: number) => {
				let CallName = pC.substring(0, pC.indexOf(' '));
				let CallArgs = pC.substring(pC.indexOf(' ')+1);
				this.ProcCallStack.filter( caller => { if(caller.procName === CallName) {
					caller.procFunc(CallArgs);
				}});
				// Should really do this after the last funciton call.
				if(i == procStack.length - 1) {
					CallBack();
				}
			});
		}
	}

	private CommitChange(callStack: CallStack, Callback: any) {
		this.ChangesIssued++;
		let DateTime = date.format(new Date(), 'YYYY-MM-DD HH:mm:ss');
		let hash = md5(callStack.sql);
		if(this.ErrorMessage == '') {
			this.Sql.get('SELECT COUNT(*) AS c FROM CHANGELOG WHERE id = '+callStack.id, (err: any, row: any) => {
				if(row['c'] === 0) {
					this.Sql.exec(callStack.sql, (err: any) => {
						this.ChangesCompleted++;
						if(err) {
							this.SetError('Error in change number '+callStack.id+' in "'+callStack.file+'"\n'+ err.message, Callback);
						} else {
							console.log('SQL [%d]: %s %s\n%s\n', callStack.id, callStack.author, callStack.file, callStack.sql);
							this.LastCompletedChangeId++;
							// An error can close the SQL just before this runs.
							if(this.Sql) {
								this.Sql.run("INSERT INTO CHANGELOG \
										(id, changeID, author, datetime, md5sum, size) \
										VALUES ((SELECT IIF(COUNT(id) == 0, 1, MAX(id)+1) AS id FROM CHANGELOG), ?, ?, ?, ?, ?);",
									callStack.id,
									callStack.author,
									DateTime,
									hash,
									callStack.sql.length
								);
							}
							this.runProcStack(callStack.procedures, () => {
								let Next = this.callStack.find( (cB: CallStack) => cB.previous == callStack.id);
								if(Next) {
									this.CommitChange(Next, Callback);
								} else {
									// This is the all done callback
									Callback(this.ErrorMessage);
								}
							});
						}
					});
				} else {
					let Next = this.callStack.find( (cB: CallStack) => { return cB.previous == callStack.id});
					if(Next) {
						this.CommitChange(Next, Callback);
					} else {
						// This the all done callback
						Callback(this.ErrorMessage);
					}
				}
			});
		}
	}

	/**
	 * 	Check the DB for the change log table.
	 *	If missing then created it and set the first change ID to zero.
	 *	Get the value highest change ID
	 */
	private ChangeLogTable(Callback: any) {
		this.Sql.get("SELECT COUNT(*) AS COUNT FROM sqlite_master WHERE type='table' AND name='CHANGELOG';", (err: any, row: any) => {
			if(err) {
				this.SetError('ChangeLogTable '+err.message, Callback);
			} else {
				if(row.COUNT) {
					this.Sql.get("SELECT IIF(MAX(changeID) == 0, 0, MAX(changeID)) AS id FROM CHANGELOG;", (err: any, row: any) => {
						if(err?.message.length > 0) {
							this.SetError(err.message, Callback);
						}
						this.LastCompletedChangeId = row.id;
					});
				} else {
					this.Sql.run("CREATE TABLE 'CHANGELOG' \
						('id' INT NOT NULL, \
						'changeID' int, \
						'author' VARCHAR(100), \
						'datetime' CHAR(20), \
						'md5sum' CHAR(32), \
						'size' INT, \
						PRIMARY KEY ('id'));");
					this.LastCompletedChangeId = 0;
					console.log('Created change log table');
				}
				Callback(this.ErrorMessage);
			}
		});
	}
}

// const sliteChange = (Sql: typeof sqlite.Database, ChangeLog: string, Callback: (string)): void => {
// 	let instanceOfSliteChange = new SliteChange(Sql, ChangeLog, Callback);
// }

// export = SliteChange;
//export * as SliteChange;
